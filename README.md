# Earthfox Privacy for Android


_Browse like no one’s watching. The new Firefox Focus automatically blocks a wide range of online trackers — from the moment you launch it to the second you leave it. Easily erase your history, passwords and cookies, so you won’t get followed by things like unwanted ads._ 

Firefox Focus provides automatic ad blocking and tracking protection on an easy-to-use private browser.

* [Download APKs](https://gitlab.bixilon.de/bixilon/earthfox-privacy-android/releases)


## Features compared to Firefox Focus
 - No Telemetry (everything patched out)

## Build Instructions


1. Clone or Download the repository:

  ```shell
  git clone https://gitlab.bixilon.de/bixilon/earthfox-privacy-android
  ```

2. Import the project into Android Studio **or** build on the command line:

  ```shell
  ./gradlew clean app:assembleEarthfoxArmDebug
  ```

3. Make sure to select the correct build variant in Android Studio:
**earthfoxArmDebug** for ARM
**earthfoxX86Debug** for X86
**earthfoxAarch64Debug** for ARM64

## Pre-push hooks
To reduce review turn-around time, we'd like all pushes to run tests locally. We'd
recommend you use our provided pre-push hook in `quality/pre-push-recommended.sh`.
Using this hook will guarantee your hook gets updated as the repository changes.
This hook tries to run as much as possible without taking too much time.

To add it, run this command from the project root:
```sh
ln -s ../../quality/pre-push-recommended.sh .git/hooks/pre-push
```

To push without running the pre-push hook (e.g. doc updates):
```sh
git push <remote> --no-verify
```

## License
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/
